# Déploiement automatique avec GitLab CI/CD

Rennes DevOps Camp 2017

![Gitlab](images/wm_no_bg.svg) <!-- .element: width="350px" -->



## ME

-

- Christophe Le Guern @c35sys
- Société PagesJaunes.fr / SoLocal
- Equipe systèmes et intégration 



# Présentation de gitLab



- Solution de gestion de versions git
- 2 versions : CE et EE
- Licence MIT pour la version CE

![MIT](images/mit.png) <!-- .element: width="125px" -->



### Fonctionnalités

-

- Repo git
- Gestion des permissions
- Demandes par projet
- Wiki
- Snippets de code
- Intègre Mattermost
- Registry pour les conteneurs
- CI/CD



# Focus sur CI/CD



## Architecture

![Architecture](images/architecture.png) <!-- .element: width="600px" -->



## Pipelines

![Pipelines](images/ci-pipelines.png)




## Le fichier gitlab-ci.yml

```
stages:
  - Build
  - Test
  - Staging
  - Production

build:
  stage: Build
  script: mvn package

test1:
  stage: Test
  script: mvn -Dtest=TestApp1 test

test2:
  stage: Test
  script: mvn -Dtest=TestApp2 test

auto-deploy-maven:
  stage: Staging
  script: ansible-playbook -i hosts deploy.yml --tags=staging

deploy to production:
  stage: Production
  allow_failure: false
  when: manual
  script: ansible-playbook -i hosts deploy.yml --tags=prod
```



## GitLab Runner
![Run](images/runner.gif)



- Agent communiquant avec le serveur GitLab
- Effectue les tâches présentes dans **gitlab-ci.yml**
- Développé par l'équipe GitLab
- Licence MIT
- Ecrit en golang -> multi-plateformes



### Runners executors

-

- Shell
- Docker
- Docker Machine (auto-scaling)
- Parallels
- VirtualBox
- SSH
- Kubernetes




# Utilisation chez PJ
![PJ](images/pj_solocal.jpg) <!-- .element: width="500px" -->



## Ansible des middlewares
![Ansible](images/ansible.png) <!-- .element: width="300px" -->



- Ansible : outil d'automatisation
- Middlewares : Nginx, Node.js, MongoDB ...
- Multi-versions : OS et middlewares

![ansible 1](images/ci-ansible1.png) <!-- .element: width="1500px" --> 



![ansible 2](images/ci-ansible2.png)



## Projets avec Terraform
![Terraform](images/terraform.png) <!-- .element: width="200px" -->



- Terraform : outil d'Infra As Code
- Infrastructure des projets
- Cloud public

![terraform 1](images/ci-terraform1.png) <!-- .element: width="1500px" -->



![terraform 2](images/ci-terraform2.png)



# Demo Time

![Demo Time](images/demotime.gif)
